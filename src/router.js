import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Marks from './views/Marks.vue'
import Profile from './views/Profile.vue'
import Bookkeeping from './views/Bookkeeping.vue'
import Lessons from './views/Lessons.vue'
import Scholarship from './views/Scholarship.vue'
import Documents from './views/Documents.vue'
import Messages from './views/Messages.vue'
import Students from './views/Students.vue'
import Store from './store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/marks',
      name: 'marks',
      component: Marks,
      //beforeEnter: AuthGuard
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
     
      
    },
    {
      path: '/bookkeeping',
      name: 'bookkeeping',
      component: Bookkeeping,
      //beforeEnter: AuthGuard
    },
    {
      path: '/lessons',
      name: 'lessons',
      component: Lessons,
      //beforeEnter: AuthGuard
    },
    {
      path: '/documents',
      name: 'documents',
      component: Documents,
      //beforeEnter: AuthGuard
    },
    {
      path: '/scholarship',
      name: 'scholarship',
      component: Scholarship,
      //beforeEnter: AuthGuard
    },
    {
      path: '/messages',
      name: 'messages',
      component: Messages,
      //beforeEnter: AuthGuard
    },
    {
      path: '/students',
      name: 'students',
      component: Students
      //beforeEnter: AuthGuard
    }

  ], mode: 'history'
})


function AuthGuard(from, to, next) {
  if (Store.getters.isUserAuthenticated)
    next()
  else
    next('/')
}