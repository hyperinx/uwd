import Vue from 'vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import VueCookies from 'vue-cookies'
import VuetifyConfirm from 'vuetify-confirm'
import { sync } from 'vuex-router-sync'

sync(store, router)

Vue.use(VuetifyConfirm, {
  buttonTrueText: 'Tak',
  buttonFalseText: 'Nie',
  width: 270
})

Vue.use(Vuetify, {
  theme: {
    primary: colors.grey.darken1,
    secondary: colors.grey.lighten4,
    accent: colors.grey.darken1
  }
})

Vue.use(VueAxios, Axios, VueCookies)
Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
