import axios from 'axios'

const URL_PREFIX = 'http://localhost:8000'
const AUTH_PREFIX = 'Bearer '

//ENDPOINTS

function login(username, password) {
  return axios.post(URL_PREFIX + '/users/login', {
    username: username,
    password: password
  })
}

function logout() {

  return axios.post(URL_PREFIX + '/users/logout', {}, {
    headers: {
        'Content-Type': 'application/json',
         Authorization: AUTH_PREFIX + localStorage.getItem("user-token")
    }
  })
}

function registration() {


}

function loadMarks(userId) {
  return axios.get(URL_PREFIX + '/lessons/marks?userId=' + userId, {
    headers:
    {
      'Content-Type': 'application/json',
      Authorization: AUTH_PREFIX + localStorage.getItem("user-token")
    }
  })
}

function loadBookkeeping() {

}

function loadDocuments() {

}

function loadLessons() {

}

function loadMessages() {

}


function loadStudentMarks(studentId){
  return axios.get(URL_PREFIX + '/marks/marks?userId=' + studentId, {
    headers:
    {
      'Content-Type': 'application/json',
      Authorization: AUTH_PREFIX + localStorage.getItem("user-token")
    }
  })
}

function loadProfile() {
  return axios.get(URL_PREFIX + '/users/profile', {
    headers:
    {
      'Content-Type': 'application/json',
      Authorization: AUTH_PREFIX + localStorage.getItem("user-token")
    }
  })
}

function loadScholarship() {

}

function loadStudents(){

  return axios.get(URL_PREFIX + '/users/mystudents', {
    headers:
    {
      'Content-Type': 'application/json',
      Authorization: AUTH_PREFIX + localStorage.getItem("user-token")
    }
  })

}


function getSessionDetails(sessionId, token) {
  return axios.get(URL_PREFIX + '/students/details?sessionId=' + sessionId, {
    headers:
    {
      Authorization: AUTH_PREFIX + token
    }
  })
}



export {
  login as Login,
  logout as Logout,
  registration as Registration,
  loadMarks as LoadMarks,
  loadBookkeeping as LoadBookkeeping,
  loadDocuments as LoadDocuments,
  loadLessons as LoadLessons,
  loadMessages as LoadMessages,
  loadProfile as LoadProfile,
  loadScholarship as LoadScholarship,
  loadStudents as LoadStudents,
  getSessionDetails as GetSessionDetails,
  loadStudentMarks as LoadStudentMarks
}