import Vue from 'vue'
import Vuex from 'vuex'
import generalModule from './store/general'
import userModule from './store/user'
import marksModule from './store/marks'
import profileModule from './store/profile'
import studentsModule from './store/students'


Vue.use(Vuex)

export default new Vuex.Store({
  modules:{
    generalModule,
    userModule,
    marksModule,
    profileModule,
    studentsModule
  }
})
