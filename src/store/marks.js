import {LoadMarks} from "../components/AxiosService"

export default {
    state: {
        marks: []
    },
    mutations: {
        SET_MARKS(state, payload){
            state.marks = payload
        }
    },
    actions: {
        LOAD_MARKS({commit}, payload){
            commit('SET_PROCESSING', true)
            LoadMarks(payload.userId, payload.token)
            .then(response => {
                commit('SET_MARKS', response.data)
            })
            .catch(function(){
                commit('SET_PROCESSING', false)
                //commit('SET_ERROR', error.response.data.validation_error)
            })
            commit('SET_PROCESSING', false)
        }
    },
    getters:{
       getMarks: (state) => state.marks
    }
  }