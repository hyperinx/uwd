import { Login } from "../components/AxiosService"
import { Logout } from "../components/AxiosService"

export default {
    state: {
        user: {
            isAuthenticated: false,
        
            
        }
    },
    mutations: {
        SET_USER(state, payload) {
            state.user.isAuthenticated = true


            /*axios.interceptors.request.use(
                function(config) {
                 const token = localStorage.getItem('user-token')
                 if (token) config.headers.Authorization = `Bearer ${token}`
                 return config
                },
                function(error) {
                 return Promise.reject(error)
                }
               )*/
        },
        UNSET_USER(state) {

            localStorage.removeItem("user-token")

            state.user = {
                isAuthenticated: false,
                groups: null,
                userId: null,
            }
        }
    },
    actions: {
        SIGN_IN({ commit }, payload) {
            commit('SET_PROCESSING', true)
            commit('CLEAR_ERROR')
            Login(payload.username, payload.password)
                .then(response => {
                    console.log(response)
                    commit('SET_USER', {
                       
                    })
                    localStorage.setItem('user-token', response.data.token)
                    commit('SET_PROCESSING', false)
                })
                .catch(function (error) {
                    localStorage.removeItem('user-token')
                    commit('SET_PROCESSING', false)
                    commit('SET_ERROR', error.response.data.validation_error)
                })
            commit('SET_PROCESSING', false)
        },
        SIGN_UP({ commit }, payload) {
            //No realization
        },

        SIGN_OUT({ commit }) {
            commit('SET_PROCESSING', true)
            Logout()
                .then(response => {
                    commit('UNSET_USER')
                })
                .catch(function (error) {
                    commit('SET_PROCESSING', false)
                })
            commit('SET_PROCESSING', false)
        },

        STATE_CHANGED({ commit, dispatch }, payload) {
            if (payload) {
                commit('SET_USER', payload.userId)
                dispatch('LOAD_USER_DATA', payload.userId)
            } else {
                commit('UNSET_USER')
                localStorage.removeItem('user-token')
            }
        }
    },
    getters: {
        isUserAuthenticated: (state) => state.user.isAuthenticated,
        getGroup: (state) => state.user.group,
        getUserId: (state) => state.user.userId,
        
    },
    rootGetters: {
        getToken: (state) => state.user.token
    }
}