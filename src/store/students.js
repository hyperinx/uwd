import { LoadStudents } from "../components/AxiosService"
import { LoadStudentMarks } from "../components/AxiosService"

export default {
    state: {
        students: [],
        studentMarks:[]
 
    },
    mutations: {
        SET_STUDENTS(state, payload) {
            state.students = payload
        },
        SET_STUDENT_MARKS(state, payload) {
            state.studentMarks = payload
        }
    },
    actions: {
        LOAD_STUDENTS({ commit }) {
            commit('SET_PROCESSING', true)
            LoadStudents()
                .then(response => {
                    console.log(response)
                    commit('SET_STUDENTS', response.data)
                })
                .catch(function () {
                    commit('SET_PROCESSING', false)
                    //commit('SET_ERROR', error.response.data.validation_error)
                })
            commit('SET_PROCESSING', false)
        },

        LOAD_STUDENT_MARKS({ commit}, payload) {
            commit('SET_PROCESSING', true)
            LoadStudentMarks(payload.user_id)
                .then(response => {
                    console.log(response)
                    commit('SET_STUDENT_MARKS', response.data)
                })
                .catch(function () {
                    commit('SET_PROCESSING', false)
                    //commit('SET_ERROR', error.response.data.validation_error)
                })      
        
            commit('SET_PROCESSING', false)
        },

        REMOVE_MARK({ commit, state }, payload){

            commit('SET_PROCESSING', true)
            commit("SET_STUDENT_EXPANDALBE_DATA", state.expandableData.filter(function( obj ) {
                return obj.field !== payload.markId;
            }))
            commit('SET_PROCESSING', false)
        }
    },
    getters: {
        getStudents: (state) => state.students,
        getStudentMarks: (state) => state.studentMarks
    }
}