import { LoadProfile } from "../components/AxiosService"

export default {
    state: {
        userProfileData: {

        }
    },
    mutations: {
        SET_USER_PROFILE_DATA(state, payload) {
            state.userProfileData = payload.userProfileData
        }
    },
    actions: {
        LOAD_PROFILE_DATA({ commit }) {
            commit('SET_PROCESSING', true)
            commit('CLEAR_ERROR')
            LoadProfile()
                .then(response => {
                    console.log(response)
                    commit('SET_USER_PROFILE_DATA', { userProfileData: response.data })
                })
                .catch(function (error) {
                    commit('SET_PROCESSING', false)
                    commit('SET_ERROR', error.response)
                })
            commit('SET_PROCESSING', false)
        },

        /*STATE_CHANGED({commit}, payload){
            if(payload){
                commit('SET_USER', payload.uid)
            }else{
                commit('UNSET_USER')
            }
        }*/
    },
    getters: {
        getUserProfileData: (state) => state.userProfileData
    }
}



